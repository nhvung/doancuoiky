﻿namespace DoAnCuoiKy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hienthi_check = new System.Windows.Forms.CheckBox();
            this.mkmau_but = new System.Windows.Forms.Button();
            this.mk2_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.mk1_txt = new System.Windows.Forms.TextBox();
            this.at_combo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.email_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dangnhap_link = new System.Windows.Forms.LinkLabel();
            this.dangky_but = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sdt_txt = new System.Windows.Forms.TextBox();
            this.diachi_txt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nam_combo = new System.Windows.Forms.ComboBox();
            this.thang_combo = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ngay_combo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.hoten_txt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.err1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.err2 = new System.Windows.Forms.ErrorProvider(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.thoat_but = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.err1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.err2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.hienthi_check);
            this.groupBox1.Controls.Add(this.mkmau_but);
            this.groupBox1.Controls.Add(this.mk2_txt);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.mk1_txt);
            this.groupBox1.Controls.Add(this.at_combo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.email_txt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(12, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(482, 163);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin đăng nhập";
            // 
            // hienthi_check
            // 
            this.hienthi_check.AutoSize = true;
            this.hienthi_check.Location = new System.Drawing.Point(114, 132);
            this.hienthi_check.Name = "hienthi_check";
            this.hienthi_check.Size = new System.Drawing.Size(107, 18);
            this.hienthi_check.TabIndex = 5;
            this.hienthi_check.Text = "Hiển thị mật khẩu";
            this.hienthi_check.UseVisualStyleBackColor = true;
            this.hienthi_check.CheckedChanged += new System.EventHandler(this.hienthi_check_CheckedChanged);
            // 
            // mkmau_but
            // 
            this.mkmau_but.Location = new System.Drawing.Point(322, 128);
            this.mkmau_but.Name = "mkmau_but";
            this.mkmau_but.Size = new System.Drawing.Size(103, 25);
            this.mkmau_but.TabIndex = 6;
            this.mkmau_but.Text = "Mật khẩu mẫu";
            this.mkmau_but.UseVisualStyleBackColor = true;
            this.mkmau_but.Click += new System.EventHandler(this.mkmau_but_Click);
            // 
            // mk2_txt
            // 
            this.mk2_txt.Location = new System.Drawing.Point(114, 100);
            this.mk2_txt.Name = "mk2_txt";
            this.mk2_txt.Size = new System.Drawing.Size(311, 20);
            this.mk2_txt.TabIndex = 4;
            this.mk2_txt.Text = "12345678910";
            this.mk2_txt.UseSystemPasswordChar = true;
            this.mk2_txt.TextChanged += new System.EventHandler(this.mk2_txt_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 14);
            this.label5.TabIndex = 6;
            this.label5.Text = "Lặp lại mật khẩu:";
            // 
            // mk1_txt
            // 
            this.mk1_txt.Location = new System.Drawing.Point(114, 72);
            this.mk1_txt.Name = "mk1_txt";
            this.mk1_txt.Size = new System.Drawing.Size(311, 20);
            this.mk1_txt.TabIndex = 3;
            this.mk1_txt.Text = "12345678910";
            this.mk1_txt.UseSystemPasswordChar = true;
            this.mk1_txt.TextChanged += new System.EventHandler(this.mk1_txt_TextChanged);
            // 
            // at_combo
            // 
            this.at_combo.FormattingEnabled = true;
            this.at_combo.Location = new System.Drawing.Point(296, 44);
            this.at_combo.Name = "at_combo";
            this.at_combo.Size = new System.Drawing.Size(129, 22);
            this.at_combo.TabIndex = 2;
            this.at_combo.Text = "student.hcmus.edu.vn";
            this.at_combo.SelectedIndexChanged += new System.EventHandler(this.at_combo_SelectedIndexChanged);
            this.at_combo.TextChanged += new System.EventHandler(this.at_combo_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "@";
            // 
            // email_txt
            // 
            this.email_txt.Location = new System.Drawing.Point(114, 44);
            this.email_txt.Name = "email_txt";
            this.email_txt.Size = new System.Drawing.Size(152, 20);
            this.email_txt.TabIndex = 1;
            this.email_txt.Text = "0912554";
            this.email_txt.TextChanged += new System.EventHandler(this.email_txt_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 14);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mật khẩu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Email:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(269, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "ĐĂNG KÝ";
            // 
            // dangnhap_link
            // 
            this.dangnhap_link.AutoSize = true;
            this.dangnhap_link.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dangnhap_link.Location = new System.Drawing.Point(284, 506);
            this.dangnhap_link.Name = "dangnhap_link";
            this.dangnhap_link.Size = new System.Drawing.Size(84, 14);
            this.dangnhap_link.TabIndex = 14;
            this.dangnhap_link.TabStop = true;
            this.dangnhap_link.Text = "Đã có tài khoản";
            this.dangnhap_link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.dangnhap_link_LinkClicked);
            // 
            // dangky_but
            // 
            this.dangky_but.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dangky_but.Location = new System.Drawing.Point(203, 501);
            this.dangky_but.Name = "dangky_but";
            this.dangky_but.Size = new System.Drawing.Size(75, 25);
            this.dangky_but.TabIndex = 13;
            this.dangky_but.Text = "Đăng ký";
            this.dangky_but.UseVisualStyleBackColor = true;
            this.dangky_but.Click += new System.EventHandler(this.dangky_but_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sdt_txt);
            this.groupBox2.Controls.Add(this.diachi_txt);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.nam_combo);
            this.groupBox2.Controls.Add(this.thang_combo);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.ngay_combo);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.hoten_txt);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 322);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(481, 164);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin cá nhân";
            // 
            // sdt_txt
            // 
            this.sdt_txt.Location = new System.Drawing.Point(114, 124);
            this.sdt_txt.Name = "sdt_txt";
            this.sdt_txt.Size = new System.Drawing.Size(311, 20);
            this.sdt_txt.TabIndex = 12;
            this.sdt_txt.Text = "01268887592";
            // 
            // diachi_txt
            // 
            this.diachi_txt.Location = new System.Drawing.Point(114, 96);
            this.diachi_txt.Name = "diachi_txt";
            this.diachi_txt.Size = new System.Drawing.Size(311, 20);
            this.diachi_txt.TabIndex = 11;
            this.diachi_txt.Text = "295 đường 3 Tháng 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(53, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 14);
            this.label13.TabIndex = 14;
            this.label13.Text = "Điện thoại:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 14);
            this.label10.TabIndex = 13;
            this.label10.Text = "Địa chỉ:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(326, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 14);
            this.label12.TabIndex = 12;
            this.label12.Text = "/";
            // 
            // nam_combo
            // 
            this.nam_combo.FormattingEnabled = true;
            this.nam_combo.Location = new System.Drawing.Point(344, 67);
            this.nam_combo.Name = "nam_combo";
            this.nam_combo.Size = new System.Drawing.Size(81, 22);
            this.nam_combo.TabIndex = 10;
            this.nam_combo.SelectedIndexChanged += new System.EventHandler(this.nam_combo_SelectedIndexChanged);
            // 
            // thang_combo
            // 
            this.thang_combo.FormattingEnabled = true;
            this.thang_combo.Location = new System.Drawing.Point(219, 67);
            this.thang_combo.Name = "thang_combo";
            this.thang_combo.Size = new System.Drawing.Size(101, 22);
            this.thang_combo.TabIndex = 9;
            this.thang_combo.SelectedIndexChanged += new System.EventHandler(this.thang_combo_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(201, 70);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 14);
            this.label11.TabIndex = 9;
            this.label11.Text = "/";
            // 
            // ngay_combo
            // 
            this.ngay_combo.FormattingEnabled = true;
            this.ngay_combo.Location = new System.Drawing.Point(114, 67);
            this.ngay_combo.Name = "ngay_combo";
            this.ngay_combo.Size = new System.Drawing.Size(81, 22);
            this.ngay_combo.TabIndex = 8;
            this.ngay_combo.SelectedIndexChanged += new System.EventHandler(this.ngay_combo_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(51, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 14);
            this.label9.TabIndex = 6;
            this.label9.Text = "Ngày sinh:";
            // 
            // hoten_txt
            // 
            this.hoten_txt.Location = new System.Drawing.Point(114, 39);
            this.hoten_txt.Name = "hoten_txt";
            this.hoten_txt.Size = new System.Drawing.Size(311, 20);
            this.hoten_txt.TabIndex = 7;
            this.hoten_txt.Text = "Nguyễn Huỳnh Vững";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 14);
            this.label7.TabIndex = 1;
            this.label7.Text = "Họ và tên:";
            // 
            // err1
            // 
            this.err1.ContainerControl = this;
            this.err1.Icon = ((System.Drawing.Icon)(resources.GetObject("err1.Icon")));
            // 
            // err2
            // 
            this.err2.ContainerControl = this;
            this.err2.Icon = ((System.Drawing.Icon)(resources.GetObject("err2.Icon")));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(35, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(152, 135);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(223, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(239, 46);
            this.label6.TabIndex = 5;
            this.label6.Text = "TÀI KHOẢN";
            // 
            // thoat_but
            // 
            this.thoat_but.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.thoat_but.Location = new System.Drawing.Point(423, 502);
            this.thoat_but.Name = "thoat_but";
            this.thoat_but.Size = new System.Drawing.Size(75, 23);
            this.thoat_but.TabIndex = 15;
            this.thoat_but.Text = "Thoát";
            this.thoat_but.UseVisualStyleBackColor = true;
            this.thoat_but.Click += new System.EventHandler(this.thoat_but_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.dangky_but;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.thoat_but;
            this.ClientSize = new System.Drawing.Size(510, 537);
            this.Controls.Add(this.thoat_but);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dangky_but);
            this.Controls.Add(this.dangnhap_link);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 400);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng ký";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.err1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.err2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel dangnhap_link;
        private System.Windows.Forms.TextBox mk2_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox mk1_txt;
        private System.Windows.Forms.ComboBox at_combo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox email_txt;
        private System.Windows.Forms.Button dangky_but;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox nam_combo;
        private System.Windows.Forms.ComboBox thang_combo;
        private System.Windows.Forms.ComboBox ngay_combo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox hoten_txt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox sdt_txt;
        private System.Windows.Forms.TextBox diachi_txt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ErrorProvider err1;
        private System.Windows.Forms.ErrorProvider err2;
        private System.Windows.Forms.Button mkmau_but;
        private System.Windows.Forms.CheckBox hienthi_check;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button thoat_but;
    }
}

