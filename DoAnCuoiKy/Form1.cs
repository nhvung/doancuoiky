﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DoAnCuoiKy
{
    public partial class Form1 : Form
    {
        private string[] mkm_ = File.ReadAllLines("passphrase_.txt");
        public string email_;
        public string matkhau_;
        public string hoten_;
        public string diachi_;
        public string sdt_;
        public string ngaysinh_;

        public Form1()
        {
            InitializeComponent();
            at_combo.Items.AddRange(File.ReadAllLines("at_.txt"));
            for (int i = 1; i <= 31; i++)
                ngay_combo.Items.Add(i);
            ngay_combo.Text = "Ngày";
            thang_combo.Text = "Tháng";
            nam_combo.Text = "Năm";
            
        }

        private void dangnhap_link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new Form2().Show();
            this.Close();
        }

        private void hienthi_check_CheckedChanged(object sender, EventArgs e)
        {
            if (!hienthi_check.Checked)
            {
                mk1_txt.UseSystemPasswordChar = true;
                mk2_txt.UseSystemPasswordChar = true;
            }
            else
            {
                mk1_txt.UseSystemPasswordChar = false;
                mk2_txt.UseSystemPasswordChar = false;
            }
        }

        private void mkmau_but_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, mkm_.Length);
            mk1_txt.Text = mkm_[index];
            mk2_txt.Text = mkm_[index];
        }

        private void ngay_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            thang_combo.Items.Clear();
            object[] mon31 = {1,3,5,7,8,10,12};
            object[] mon30 = {1,3,4,5,6,7,8,9,10,11,12};
            object[] n_mon = {1,2,3,4,5,6,7,8,9,10,11,12 };
            if (thang_combo.Text == "Tháng")
            {
                if (ngay_combo.Text == "31")
                {
                    thang_combo.Items.AddRange(mon31);
                }
                else if (ngay_combo.Text == "30")
                {
                    thang_combo.Items.AddRange(mon30);
                }
                else
                {
                    thang_combo.Items.AddRange(n_mon);
                }
            }
            else
            {
                nam_combo.Items.Clear();
                if (ngay_combo.Text == "31")
                {
                    if (thang_combo.Text == "2" || thang_combo.Text == "4" || thang_combo.Text == "6" || thang_combo.Text == "9" || thang_combo.Text == "11")
                        thang_combo.Text = "Tháng";
                    thang_combo.Items.AddRange(mon31);
                    for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                        nam_combo.Items.Add(i);
                }
                else if (ngay_combo.Text == "30")
                {
                    if (thang_combo.Text == "2")
                        thang_combo.Text = "Tháng";
                    thang_combo.Items.AddRange(mon30);
                    for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                        nam_combo.Items.Add(i);
                }
                else if (ngay_combo.Text == "29" && thang_combo.Text == "2")
                {
                    if (nam_combo.Text != "Năm" && Int32.Parse(nam_combo.Text) % 4 != 0)
                    {
                        nam_combo.Text = "Năm";
                    }
                    thang_combo.Items.AddRange(n_mon);
                    for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                        if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                            nam_combo.Items.Add(i);
                }
                else
                {
                    thang_combo.Items.AddRange(n_mon);
                    for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                        nam_combo.Items.Add(i);
                }
            }
            if (ngay_combo.Text != "Ngày" && thang_combo.Text != "Tháng" && nam_combo.Text != "Năm")
                err1.Clear();
        }

        private void thang_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            nam_combo.Items.Clear();
            if (ngay_combo.Text == "29" && thang_combo.Text == "2")
            {
                if (nam_combo.Text != "Năm" && Int32.Parse(nam_combo.Text) % 4 != 0)
                {
                    nam_combo.Text = "Năm";
                }
                for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                {
                    if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0)
                    {
                        nam_combo.Items.Add(i);
                    }
                }
            }
            else
            {
                for (int i = DateTime.Now.Year - 10; i >= DateTime.Now.Year - 100; i--)
                    nam_combo.Items.Add(i);
            }
            if (ngay_combo.Text != "Ngày" && thang_combo.Text != "Tháng" && nam_combo.Text != "Năm")
                err1.Clear();
        }

        private void mk1_txt_TextChanged(object sender, EventArgs e)
        {
            if (mk1_txt.Text.Length < 10)
            {
                err1.SetError(mk1_txt, "Mật khẩu không đủ dài! Vui lòng nhập mật khẩu dài hơn 10 ký tự");
                err2.Clear();
            }
            else
            {
                err1.Clear();
                err2.SetError(mk1_txt, "OK");
            }
        }

        private void mk2_txt_TextChanged(object sender, EventArgs e)
        {
            if (mk2_txt.Text.Length < 10 && mk2_txt.Text != mk1_txt.Text)
            {
                err1.SetError(mk2_txt, "Mật khẩu nhập lại không khớp với mật khẩu trên hoặc chưa đủ độ dài tối thiểu");
                err2.Clear();
            }
            else
            {
                err1.Clear();
                err2.SetError(mk2_txt, "OK");
            }
        }

        private void dangky_but_Click(object sender, EventArgs e)
        {
            if (email_txt.Text == "" || at_combo.Text == "")
                err1.SetError(at_combo, "Email không hợp lệ");
            else
            {
                err1.Clear();
                if (mk1_txt.Text == "")
                    err1.SetError(mk1_txt, "Chưa nhập mật khẩu");
                else
                {
                    err1.Clear();
                    if (mk2_txt.Text != mk1_txt.Text)
                        err1.SetError(mk2_txt, "Mật khẩu nhập lại không khớp");
                    else
                    {
                        err1.Clear();
                        if (hoten_txt.Text == "")
                            err1.SetError(hoten_txt, "Không được bỏ trổng họ tên");
                        else
                        {
                            err1.Clear();
                            if (ngay_combo.Text == "Ngày" || thang_combo.Text == "Tháng" || nam_combo.Text == "Năm")
                                err1.SetError(nam_combo, "Chưa nhập ngày tháng năm sinh");
                            else
                            {
                                err1.Clear();
                                if (diachi_txt.Text == "")
                                    err1.SetError(diachi_txt, "Chưa nhập địa chỉ");
                                else
                                {
                                    err1.Clear();
                                    if (sdt_txt.Text == "")
                                        err1.SetError(sdt_txt, "Chưa nhập số điện thoại");
                                    else
                                    {
                                        err1.Clear();
                                        // Thong tin dang nhap
                                        email_ = email_txt.Text + "@" + at_combo.Text;
                                        if (File.Exists("Accounts\\" + email_.Replace("@", "_") + ".acc"))
                                            MessageBox.Show("Email này đã có người sử dụng");
                                        else
                                        {
                                            matkhau_ = mk1_txt.Text;

                                            // Thong tin ca nhan
                                            hoten_ = hoten_txt.Text;
                                            ngaysinh_ = ngay_combo.Text + "/" + thang_combo.Text + "/" + nam_combo.Text;
                                            diachi_ = diachi_txt.Text;
                                            sdt_ = sdt_txt.Text;
                                            string ttcanhan = hoten_ + "<<" + ngaysinh_ + "<<" + diachi_ + "<<" + sdt_;

                                            // Chuyen den tao khoa
                                            new Form3(hoten_, ngaysinh_, diachi_, sdt_, email_, matkhau_).Show();
                                            this.Close();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void email_txt_TextChanged(object sender, EventArgs e)
        {
            if (email_txt.Text == "" || at_combo.Text == "")
                err1.SetError(at_combo, "Email không hợp lệ");
            else
                err1.Clear();
        }

        private void at_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (email_txt.Text == "" || at_combo.Text == "")
                err1.SetError(at_combo, "Email không hợp lệ");
            else
                err1.Clear();
        }

        private void at_combo_TextChanged(object sender, EventArgs e)
        {
            if (email_txt.Text == "" || at_combo.Text == "")
                err1.SetError(at_combo, "Email không hợp lệ");
            else
                err1.Clear();
        }

        private void thoat_but_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát không", "Thoát", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Application.Exit();
        }

        private void nam_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ngay_combo.Text != "Ngày" && thang_combo.Text != "Tháng" && nam_combo.Text != "Năm")
                err1.Clear();
        }
    }
}
