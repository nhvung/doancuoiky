﻿namespace DoAnCuoiKy
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label1 = new System.Windows.Forms.Label();
            this.email_txt = new System.Windows.Forms.TextBox();
            this.matkhau_txt = new System.Windows.Forms.TextBox();
            this.dangky_link = new System.Windows.Forms.LinkLabel();
            this.dangnhap_but = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.thoat_but = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 363);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Email:";
            // 
            // email_txt
            // 
            this.email_txt.Location = new System.Drawing.Point(100, 360);
            this.email_txt.Name = "email_txt";
            this.email_txt.Size = new System.Drawing.Size(346, 20);
            this.email_txt.TabIndex = 2;
            this.email_txt.Text = "nhvung@live.com";
            // 
            // matkhau_txt
            // 
            this.matkhau_txt.Location = new System.Drawing.Point(99, 386);
            this.matkhau_txt.Name = "matkhau_txt";
            this.matkhau_txt.Size = new System.Drawing.Size(346, 20);
            this.matkhau_txt.TabIndex = 3;
            this.matkhau_txt.Text = "Vunghuynh1";
            this.matkhau_txt.UseSystemPasswordChar = true;
            // 
            // dangky_link
            // 
            this.dangky_link.AutoSize = true;
            this.dangky_link.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dangky_link.Location = new System.Drawing.Point(277, 427);
            this.dangky_link.Name = "dangky_link";
            this.dangky_link.Size = new System.Drawing.Size(97, 14);
            this.dangky_link.TabIndex = 4;
            this.dangky_link.TabStop = true;
            this.dangky_link.Text = "Chưa có tài khoản";
            this.dangky_link.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.dangky_link_LinkClicked);
            // 
            // dangnhap_but
            // 
            this.dangnhap_but.Location = new System.Drawing.Point(196, 423);
            this.dangnhap_but.Name = "dangnhap_but";
            this.dangnhap_but.Size = new System.Drawing.Size(75, 23);
            this.dangnhap_but.TabIndex = 5;
            this.dangnhap_but.Text = "Đăng nhập";
            this.dangnhap_but.UseVisualStyleBackColor = true;
            this.dangnhap_but.Click += new System.EventHandler(this.dangnhap_but_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label2.Location = new System.Drawing.Point(152, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 32);
            this.label2.TabIndex = 6;
            this.label2.Text = "ĐĂNG NHẬP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 389);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mật khẩu:";
            // 
            // thoat_but
            // 
            this.thoat_but.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.thoat_but.Location = new System.Drawing.Point(423, 502);
            this.thoat_but.Name = "thoat_but";
            this.thoat_but.Size = new System.Drawing.Size(75, 23);
            this.thoat_but.TabIndex = 8;
            this.thoat_but.Text = "Thoát";
            this.thoat_but.UseVisualStyleBackColor = true;
            this.thoat_but.Click += new System.EventHandler(this.thoat_but_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(158, 59);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 188);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Form2
            // 
            this.AcceptButton = this.dangnhap_but;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.thoat_but;
            this.ClientSize = new System.Drawing.Size(510, 537);
            this.Controls.Add(this.thoat_but);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dangnhap_but);
            this.Controls.Add(this.dangky_link);
            this.Controls.Add(this.matkhau_txt);
            this.Controls.Add(this.email_txt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 400);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng nhập";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox email_txt;
        private System.Windows.Forms.TextBox matkhau_txt;
        private System.Windows.Forms.LinkLabel dangky_link;
        private System.Windows.Forms.Button dangnhap_but;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button thoat_but;
    }
}