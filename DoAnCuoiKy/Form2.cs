﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace DoAnCuoiKy
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        

        private void thoat_but_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát không", "Thoát", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Application.Exit();
        }

        private void dangnhap_but_Click(object sender, EventArgs e)
        {
            if (email_txt.Text == "")
            {
                MessageBox.Show("Chưa nhập email");
            }
            else if (matkhau_txt.Text == "")
            {
                MessageBox.Show("Chưa nhập mật khẩu");
            }
            else
            {
                string email = email_txt.Text;
                string matkhau = matkhau_txt.Text;
                string filename = "Accounts\\" + BitConverter.ToString(new SHA1CryptoServiceProvider().ComputeHash(Encoding.Unicode.GetBytes(email + matkhau))).Replace("-", "") + ".acc";
                if (File.Exists(filename))
                {
                    string[] thongtin = File.ReadAllLines(filename);
                    byte[] salt = Convert.FromBase64String(thongtin[7]);

                }
            }
        }

        private void dangky_link_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new Form1().Show();
            this.Close();
        }
    }
}
