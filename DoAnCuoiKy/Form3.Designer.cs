﻿namespace DoAnCuoiKy
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ddkhoa_combo = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ketiep_but = new System.Windows.Forms.Button();
            this.trolai_but = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.matkhau_ = new System.Windows.Forms.Label();
            this.email_ = new System.Windows.Forms.Label();
            this.sdt_ = new System.Windows.Forms.Label();
            this.diachi_ = new System.Windows.Forms.Label();
            this.ngaysinh_ = new System.Windows.Forms.Label();
            this.hoten_ = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.thoat_but = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DoAnCuoiKy.Properties.Resources.keys;
            this.pictureBox1.Location = new System.Drawing.Point(26, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label1.Location = new System.Drawing.Point(208, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 32);
            this.label1.TabIndex = 1;
            this.label1.Text = "PHÁT SINH KHÓA";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 14);
            this.label2.TabIndex = 2;
            this.label2.Text = "Độ dài khóa:";
            // 
            // ddkhoa_combo
            // 
            this.ddkhoa_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddkhoa_combo.FormattingEnabled = true;
            this.ddkhoa_combo.Location = new System.Drawing.Point(190, 34);
            this.ddkhoa_combo.Name = "ddkhoa_combo";
            this.ddkhoa_combo.Size = new System.Drawing.Size(121, 22);
            this.ddkhoa_combo.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.ddkhoa_combo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 388);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 78);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin khóa";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(317, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 14);
            this.label9.TabIndex = 4;
            this.label9.Text = "bits";
            // 
            // ketiep_but
            // 
            this.ketiep_but.Location = new System.Drawing.Point(342, 502);
            this.ketiep_but.Name = "ketiep_but";
            this.ketiep_but.Size = new System.Drawing.Size(75, 23);
            this.ketiep_but.TabIndex = 7;
            this.ketiep_but.Text = "Kế tiếp >>";
            this.ketiep_but.UseVisualStyleBackColor = true;
            this.ketiep_but.Click += new System.EventHandler(this.ketiep_but_Click);
            // 
            // trolai_but
            // 
            this.trolai_but.Location = new System.Drawing.Point(261, 502);
            this.trolai_but.Name = "trolai_but";
            this.trolai_but.Size = new System.Drawing.Size(75, 23);
            this.trolai_but.TabIndex = 8;
            this.trolai_but.Text = "<< Trở lại";
            this.trolai_but.UseVisualStyleBackColor = true;
            this.trolai_but.Click += new System.EventHandler(this.trolai_but_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.matkhau_);
            this.groupBox2.Controls.Add(this.email_);
            this.groupBox2.Controls.Add(this.sdt_);
            this.groupBox2.Controls.Add(this.diachi_);
            this.groupBox2.Controls.Add(this.ngaysinh_);
            this.groupBox2.Controls.Add(this.hoten_);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(486, 236);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin tài khoản";
            // 
            // matkhau_
            // 
            this.matkhau_.AutoSize = true;
            this.matkhau_.Location = new System.Drawing.Point(174, 182);
            this.matkhau_.Name = "matkhau_";
            this.matkhau_.Size = new System.Drawing.Size(0, 14);
            this.matkhau_.TabIndex = 11;
            // 
            // email_
            // 
            this.email_.AutoSize = true;
            this.email_.Location = new System.Drawing.Point(174, 152);
            this.email_.Name = "email_";
            this.email_.Size = new System.Drawing.Size(0, 14);
            this.email_.TabIndex = 10;
            // 
            // sdt_
            // 
            this.sdt_.AutoSize = true;
            this.sdt_.Location = new System.Drawing.Point(174, 124);
            this.sdt_.Name = "sdt_";
            this.sdt_.Size = new System.Drawing.Size(0, 14);
            this.sdt_.TabIndex = 9;
            // 
            // diachi_
            // 
            this.diachi_.AutoSize = true;
            this.diachi_.Location = new System.Drawing.Point(174, 98);
            this.diachi_.Name = "diachi_";
            this.diachi_.Size = new System.Drawing.Size(0, 14);
            this.diachi_.TabIndex = 8;
            // 
            // ngaysinh_
            // 
            this.ngaysinh_.AutoSize = true;
            this.ngaysinh_.Location = new System.Drawing.Point(174, 68);
            this.ngaysinh_.Name = "ngaysinh_";
            this.ngaysinh_.Size = new System.Drawing.Size(0, 14);
            this.ngaysinh_.TabIndex = 7;
            // 
            // hoten_
            // 
            this.hoten_.AutoSize = true;
            this.hoten_.Location = new System.Drawing.Point(174, 39);
            this.hoten_.Name = "hoten_";
            this.hoten_.Size = new System.Drawing.Size(0, 14);
            this.hoten_.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(87, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 14);
            this.label8.TabIndex = 5;
            this.label8.Text = "Mật khẩu:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(106, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 14);
            this.label7.TabIndex = 4;
            this.label7.Text = "Email:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 14);
            this.label6.TabIndex = 3;
            this.label6.Text = "Số điện thoại:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(97, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 14);
            this.label5.TabIndex = 2;
            this.label5.Text = "Địa chỉ:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 14);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ngày sinh:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Họ và tên:";
            // 
            // thoat_but
            // 
            this.thoat_but.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.thoat_but.Location = new System.Drawing.Point(423, 502);
            this.thoat_but.Name = "thoat_but";
            this.thoat_but.Size = new System.Drawing.Size(75, 23);
            this.thoat_but.TabIndex = 10;
            this.thoat_but.Text = "Thoát";
            this.thoat_but.UseVisualStyleBackColor = true;
            this.thoat_but.Click += new System.EventHandler(this.thoat_but_Click);
            // 
            // Form3
            // 
            this.AcceptButton = this.ketiep_but;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.thoat_but;
            this.ClientSize = new System.Drawing.Size(510, 537);
            this.Controls.Add(this.thoat_but);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.trolai_but);
            this.Controls.Add(this.ketiep_but);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 400);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phát sinh khóa";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddkhoa_combo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ketiep_but;
        private System.Windows.Forms.Button trolai_but;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label email_;
        private System.Windows.Forms.Label sdt_;
        private System.Windows.Forms.Label diachi_;
        private System.Windows.Forms.Label ngaysinh_;
        private System.Windows.Forms.Label hoten_;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label matkhau_;
        private System.Windows.Forms.Button thoat_but;
        private System.Windows.Forms.Label label9;
    }
}