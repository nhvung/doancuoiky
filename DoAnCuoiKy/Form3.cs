﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Xml;

namespace DoAnCuoiKy
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            for (int i = 512; i <= 1024; i += 64)
                ddkhoa_combo.Items.Add(i);
        }
        public Form3(string hoten, string ngaysinh, string diachi, string sdt, string email, string matkhau)
        {
            InitializeComponent();
            
            hoten_.Text = hoten;
            ngaysinh_.Text = ngaysinh;
            diachi_.Text = diachi;
            sdt_.Text = sdt;
            email_.Text = email;
            matkhau_.Text = matkhau;

            for (int i = 512; i <= 1024; i += 64)
                ddkhoa_combo.Items.Add(i);
        }

        // Cap khoa
        private RSA rsa;

        private void trolai_but_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Close();
        }

        private void taokhoa_but_Click(object sender, EventArgs e)
        {
            if (ddkhoa_combo.Text != "")
            {
                int sz = Int32.Parse(ddkhoa_combo.Text);
                rsa = new RSACryptoServiceProvider(sz, new CspParameters(1));
                MessageBox.Show("Khóa đã được tạo\n\nKhuyến cáo:\nBạn nên lưu lại khóa ngay bằng cách chọn nút 'Lưu' bên dưới");
            }
        }

        private void thoat_but_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát không", "Thoát", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Application.Exit();
        }
       
        private void ketiep_but_Click(object sender, EventArgs e)
        {
            if (ddkhoa_combo.Text == "")
            {
                MessageBox.Show("Chưa chọn độ dài của khóa");
            }
            else
            {

                
                
                // Tong hop thong tin
                string[] thongtin = new string[2];

                // Cap khoa
                int sz = Int32.Parse(ddkhoa_combo.Text);
                rsa = new RSACryptoServiceProvider(sz, new CspParameters(1));

                // Public key
                thongtin[0] = rsa.ToXmlString(false);
                // Private key
               
                

                thongtin[1] = rsa.ToXmlString(true);

                // Salt
                byte[] salt = new byte[sz];
                new RNGCryptoServiceProvider().GetBytes(salt);

                // Passphrase
                byte[] passphrase = Encoding.Unicode.GetBytes(matkhau_.Text);

                // Passphrase + Salt
                byte[] PassphraseAndSalt = new byte[salt.Length + passphrase.Length];
                for (int i = 0; i < passphrase.Length; i++)
                    PassphraseAndSalt[i] = passphrase[i];
                for (int i = passphrase.Length; i < passphrase.Length + salt.Length; i++)
                    PassphraseAndSalt[i] = salt[i - passphrase.Length];

                // Hash Passphrase + Salt
                PassphraseAndSalt = new SHA1CryptoServiceProvider().ComputeHash(PassphraseAndSalt);

                string filename = "Accounts\\temp.acc";
                File.WriteAllLines(filename, thongtin);
                new Form4().Show();
                this.Close();
            }
        }
    }
}
