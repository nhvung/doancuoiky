﻿namespace DoAnCuoiKy
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form5));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.matkhaumoi2_txt = new System.Windows.Forms.TextBox();
            this.matkhaumoi1_txt = new System.Windows.Forms.TextBox();
            this.matkhaumoi2_lab = new System.Windows.Forms.Label();
            this.matkhaumoi1_lab = new System.Windows.Forms.Label();
            this.tt_matkhau_lab = new System.Windows.Forms.Label();
            this.doi_matkhau_but = new System.Windows.Forms.Button();
            this.tt_email_lab = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sua_but = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tt_sdt_txt = new System.Windows.Forms.TextBox();
            this.tt_diachi_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tt_ngaysinh_txt = new System.Windows.Forms.TextBox();
            this.tt_hoten_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.thoat_but = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.nx_nhapkhoa_but = new System.Windows.Forms.Button();
            this.nx_xuatkhoa_but = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mh_f_nhap_txt = new System.Windows.Forms.TextBox();
            this.mh_duyet_but = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.mh_moo_combo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.mh_but = new System.Windows.Forms.Button();
            this.mh_ps_combo = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(486, 525);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(478, 498);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông tin";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.matkhaumoi2_txt);
            this.groupBox2.Controls.Add(this.matkhaumoi1_txt);
            this.groupBox2.Controls.Add(this.matkhaumoi2_lab);
            this.groupBox2.Controls.Add(this.matkhaumoi1_lab);
            this.groupBox2.Controls.Add(this.tt_matkhau_lab);
            this.groupBox2.Controls.Add(this.doi_matkhau_but);
            this.groupBox2.Controls.Add(this.tt_email_lab);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(17, 278);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(438, 202);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin tài khoản";
            // 
            // matkhaumoi2_txt
            // 
            this.matkhaumoi2_txt.Location = new System.Drawing.Point(132, 118);
            this.matkhaumoi2_txt.Name = "matkhaumoi2_txt";
            this.matkhaumoi2_txt.Size = new System.Drawing.Size(257, 20);
            this.matkhaumoi2_txt.TabIndex = 16;
            this.matkhaumoi2_txt.UseSystemPasswordChar = true;
            this.matkhaumoi2_txt.Visible = false;
            // 
            // matkhaumoi1_txt
            // 
            this.matkhaumoi1_txt.Location = new System.Drawing.Point(132, 92);
            this.matkhaumoi1_txt.Name = "matkhaumoi1_txt";
            this.matkhaumoi1_txt.Size = new System.Drawing.Size(257, 20);
            this.matkhaumoi1_txt.TabIndex = 15;
            this.matkhaumoi1_txt.UseSystemPasswordChar = true;
            this.matkhaumoi1_txt.Visible = false;
            // 
            // matkhaumoi2_lab
            // 
            this.matkhaumoi2_lab.AutoSize = true;
            this.matkhaumoi2_lab.Location = new System.Drawing.Point(12, 121);
            this.matkhaumoi2_lab.Name = "matkhaumoi2_lab";
            this.matkhaumoi2_lab.Size = new System.Drawing.Size(114, 14);
            this.matkhaumoi2_lab.TabIndex = 14;
            this.matkhaumoi2_lab.Text = "Nhập lại mật khẩu mới:";
            this.matkhaumoi2_lab.Visible = false;
            // 
            // matkhaumoi1_lab
            // 
            this.matkhaumoi1_lab.AutoSize = true;
            this.matkhaumoi1_lab.Location = new System.Drawing.Point(53, 95);
            this.matkhaumoi1_lab.Name = "matkhaumoi1_lab";
            this.matkhaumoi1_lab.Size = new System.Drawing.Size(73, 14);
            this.matkhaumoi1_lab.TabIndex = 13;
            this.matkhaumoi1_lab.Text = "Mật khẩu mới:";
            this.matkhaumoi1_lab.Visible = false;
            // 
            // tt_matkhau_lab
            // 
            this.tt_matkhau_lab.AutoSize = true;
            this.tt_matkhau_lab.Location = new System.Drawing.Point(129, 66);
            this.tt_matkhau_lab.Name = "tt_matkhau_lab";
            this.tt_matkhau_lab.Size = new System.Drawing.Size(87, 14);
            this.tt_matkhau_lab.TabIndex = 12;
            this.tt_matkhau_lab.Text = "********************";
            // 
            // doi_matkhau_but
            // 
            this.doi_matkhau_but.Location = new System.Drawing.Point(162, 161);
            this.doi_matkhau_but.Name = "doi_matkhau_but";
            this.doi_matkhau_but.Size = new System.Drawing.Size(123, 23);
            this.doi_matkhau_but.TabIndex = 11;
            this.doi_matkhau_but.Text = "Đổi mật khẩu";
            this.doi_matkhau_but.UseVisualStyleBackColor = true;
            this.doi_matkhau_but.Click += new System.EventHandler(this.doi_matkhau_but_Click);
            // 
            // tt_email_lab
            // 
            this.tt_email_lab.AutoSize = true;
            this.tt_email_lab.Location = new System.Drawing.Point(129, 34);
            this.tt_email_lab.Name = "tt_email_lab";
            this.tt_email_lab.Size = new System.Drawing.Size(35, 14);
            this.tt_email_lab.TabIndex = 1;
            this.tt_email_lab.Text = "label7";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(70, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 14);
            this.label5.TabIndex = 9;
            this.label5.Text = "Mật khẩu:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(89, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Email:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sua_but);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tt_sdt_txt);
            this.groupBox1.Controls.Add(this.tt_diachi_txt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tt_ngaysinh_txt);
            this.groupBox1.Controls.Add(this.tt_hoten_txt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(17, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 243);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin cá nhân";
            // 
            // sua_but
            // 
            this.sua_but.Location = new System.Drawing.Point(181, 191);
            this.sua_but.Name = "sua_but";
            this.sua_but.Size = new System.Drawing.Size(75, 23);
            this.sua_but.TabIndex = 8;
            this.sua_but.Text = "Chỉnh sửa";
            this.sua_but.UseVisualStyleBackColor = true;
            this.sua_but.Click += new System.EventHandler(this.sua_but_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 14);
            this.label4.TabIndex = 7;
            this.label4.Text = "Điện thoại:";
            // 
            // tt_sdt_txt
            // 
            this.tt_sdt_txt.Enabled = false;
            this.tt_sdt_txt.Location = new System.Drawing.Point(97, 142);
            this.tt_sdt_txt.Name = "tt_sdt_txt";
            this.tt_sdt_txt.Size = new System.Drawing.Size(305, 20);
            this.tt_sdt_txt.TabIndex = 6;
            // 
            // tt_diachi_txt
            // 
            this.tt_diachi_txt.Enabled = false;
            this.tt_diachi_txt.Location = new System.Drawing.Point(97, 76);
            this.tt_diachi_txt.Multiline = true;
            this.tt_diachi_txt.Name = "tt_diachi_txt";
            this.tt_diachi_txt.Size = new System.Drawing.Size(305, 60);
            this.tt_diachi_txt.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "Địa chỉ:";
            // 
            // tt_ngaysinh_txt
            // 
            this.tt_ngaysinh_txt.Enabled = false;
            this.tt_ngaysinh_txt.Location = new System.Drawing.Point(97, 50);
            this.tt_ngaysinh_txt.Name = "tt_ngaysinh_txt";
            this.tt_ngaysinh_txt.Size = new System.Drawing.Size(305, 20);
            this.tt_ngaysinh_txt.TabIndex = 3;
            // 
            // tt_hoten_txt
            // 
            this.tt_hoten_txt.Enabled = false;
            this.tt_hoten_txt.Location = new System.Drawing.Point(97, 24);
            this.tt_hoten_txt.Name = "tt_hoten_txt";
            this.tt_hoten_txt.Size = new System.Drawing.Size(305, 20);
            this.tt_hoten_txt.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày sinh:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Họ và tên:";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(478, 498);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Nhập xuất thông tin cặp khóa";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(478, 498);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Mã hóa và Giải mã";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(478, 498);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Ký và xác nhận chữ ký";
            // 
            // thoat_but
            // 
            this.thoat_but.Location = new System.Drawing.Point(423, 543);
            this.thoat_but.Name = "thoat_but";
            this.thoat_but.Size = new System.Drawing.Size(75, 23);
            this.thoat_but.TabIndex = 1;
            this.thoat_but.Text = "Thoát";
            this.thoat_but.UseVisualStyleBackColor = true;
            this.thoat_but.Click += new System.EventHandler(this.thoat_but_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Location = new System.Drawing.Point(6, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(466, 476);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nhập/Xuất thông tin khóa";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.nx_nhapkhoa_but);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(454, 100);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Nhập thông tin khóa";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.nx_xuatkhoa_but);
            this.groupBox5.Location = new System.Drawing.Point(6, 138);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(454, 93);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Xuất thông tin khóa";
            // 
            // nx_nhapkhoa_but
            // 
            this.nx_nhapkhoa_but.Location = new System.Drawing.Point(181, 43);
            this.nx_nhapkhoa_but.Name = "nx_nhapkhoa_but";
            this.nx_nhapkhoa_but.Size = new System.Drawing.Size(75, 23);
            this.nx_nhapkhoa_but.TabIndex = 2;
            this.nx_nhapkhoa_but.Text = "Nhập";
            this.nx_nhapkhoa_but.UseVisualStyleBackColor = true;
            this.nx_nhapkhoa_but.Click += new System.EventHandler(this.nx_nhapkhoa_but_Click);
            // 
            // nx_xuatkhoa_but
            // 
            this.nx_xuatkhoa_but.Location = new System.Drawing.Point(181, 32);
            this.nx_xuatkhoa_but.Name = "nx_xuatkhoa_but";
            this.nx_xuatkhoa_but.Size = new System.Drawing.Size(75, 23);
            this.nx_xuatkhoa_but.TabIndex = 3;
            this.nx_xuatkhoa_but.Text = "Xuất";
            this.nx_xuatkhoa_but.UseVisualStyleBackColor = true;
            this.nx_xuatkhoa_but.Click += new System.EventHandler(this.nx_xuatkhoa_but_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 140);
            this.label9.TabIndex = 0;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.mh_but);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.mh_duyet_but);
            this.groupBox6.Controls.Add(this.mh_f_nhap_txt);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Location = new System.Drawing.Point(6, 24);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(466, 252);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Mã hóa tập tin";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chọn (các) tập tin cần mã hóa:";
            // 
            // mh_f_nhap_txt
            // 
            this.mh_f_nhap_txt.Location = new System.Drawing.Point(19, 62);
            this.mh_f_nhap_txt.Name = "mh_f_nhap_txt";
            this.mh_f_nhap_txt.Size = new System.Drawing.Size(347, 20);
            this.mh_f_nhap_txt.TabIndex = 1;
            // 
            // mh_duyet_but
            // 
            this.mh_duyet_but.Location = new System.Drawing.Point(372, 61);
            this.mh_duyet_but.Name = "mh_duyet_but";
            this.mh_duyet_but.Size = new System.Drawing.Size(75, 23);
            this.mh_duyet_but.TabIndex = 2;
            this.mh_duyet_but.Text = "Duyệt";
            this.mh_duyet_but.UseVisualStyleBackColor = true;
            this.mh_duyet_but.Click += new System.EventHandler(this.mh_duyet_but_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 14);
            this.label8.TabIndex = 3;
            this.label8.Text = "Mode of Operations:";
            // 
            // mh_moo_combo
            // 
            this.mh_moo_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mh_moo_combo.FormattingEnabled = true;
            this.mh_moo_combo.Items.AddRange(new object[] {
            "ECB",
            "CBC",
            "OFB",
            "CFB",
            "CTS"});
            this.mh_moo_combo.Location = new System.Drawing.Point(199, 26);
            this.mh_moo_combo.Name = "mh_moo_combo";
            this.mh_moo_combo.Size = new System.Drawing.Size(121, 22);
            this.mh_moo_combo.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(103, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 14);
            this.label10.TabIndex = 5;
            this.label10.Text = "Padding Scheme:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.mh_ps_combo);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.mh_moo_combo);
            this.groupBox7.Controls.Add(this.label8);
            this.groupBox7.Location = new System.Drawing.Point(19, 98);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(428, 96);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Tùy chọn cho khóa đối xứng";
            // 
            // mh_but
            // 
            this.mh_but.Location = new System.Drawing.Point(194, 209);
            this.mh_but.Name = "mh_but";
            this.mh_but.Size = new System.Drawing.Size(75, 23);
            this.mh_but.TabIndex = 7;
            this.mh_but.Text = "Mã hóa";
            this.mh_but.UseVisualStyleBackColor = true;
            this.mh_but.Click += new System.EventHandler(this.mh_but_Click);
            // 
            // mh_ps_combo
            // 
            this.mh_ps_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mh_ps_combo.FormattingEnabled = true;
            this.mh_ps_combo.Items.AddRange(new object[] {
            "X923",
            "PKCS7",
            "ISO10126"});
            this.mh_ps_combo.Location = new System.Drawing.Point(199, 54);
            this.mh_ps_combo.Name = "mh_ps_combo";
            this.mh_ps_combo.Size = new System.Drawing.Size(121, 22);
            this.mh_ps_combo.TabIndex = 6;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 578);
            this.Controls.Add(this.thoat_but);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 400);
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button thoat_but;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button sua_but;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tt_sdt_txt;
        private System.Windows.Forms.TextBox tt_diachi_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tt_ngaysinh_txt;
        private System.Windows.Forms.TextBox tt_hoten_txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button doi_matkhau_but;
        private System.Windows.Forms.Label tt_email_lab;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox matkhaumoi2_txt;
        private System.Windows.Forms.TextBox matkhaumoi1_txt;
        private System.Windows.Forms.Label matkhaumoi2_lab;
        private System.Windows.Forms.Label matkhaumoi1_lab;
        private System.Windows.Forms.Label tt_matkhau_lab;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button nx_xuatkhoa_but;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button nx_nhapkhoa_but;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button mh_but;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox mh_ps_combo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox mh_moo_combo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button mh_duyet_but;
        private System.Windows.Forms.TextBox mh_f_nhap_txt;
        private System.Windows.Forms.Label label7;
    }
}