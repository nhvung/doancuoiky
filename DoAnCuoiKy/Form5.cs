﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace DoAnCuoiKy
{
    public partial class Form5 : Form
    {
        private string matkhau_;
        public Form5()
        {
            InitializeComponent();
        }
        public Form5(string email, string matkhau)
        {
            InitializeComponent();
            this.Text = email;
            tt_email_lab.Text = email;
            matkhau_ = matkhau;
            KhoiTao();
        }
        private string[] thongtin;
        private string[] files;

        private void KhoiTao()
        {
            thongtin = File.ReadAllLines("Accounts\\" + this.Text.Replace("@","_") + ".acc");
            tt_hoten_txt.Text = thongtin[3];
            tt_ngaysinh_txt.Text = thongtin[4];
            tt_diachi_txt.Text = thongtin[5];
            tt_sdt_txt.Text = thongtin[6];
        }

        private void sua_but_Click(object sender, EventArgs e)
        {
            if (sua_but.Text == "Chỉnh sửa")
            {
                Form6 f6 = new Form6();
                f6.ShowDialog();
                if (f6.matkhau_ == matkhau_)
                {
                    tt_hoten_txt.Enabled = true;
                    tt_ngaysinh_txt.Enabled = true;
                    tt_diachi_txt.Enabled = true;
                    tt_sdt_txt.Enabled = true;
                    sua_but.Text = "Xong";
                }
                else
                {
                    MessageBox.Show("Sai mật khẩu");
                }
            }
            else if (sua_but.Text == "Xong")
            {
                thongtin[3] = tt_hoten_txt.Text;
                thongtin[4] = tt_ngaysinh_txt.Text;
                thongtin[5] = tt_diachi_txt.Text;
                thongtin[6] = tt_sdt_txt.Text;
                File.WriteAllLines("Accounts\\" + this.Text.Replace("@", "_") + ".acc", thongtin);
                sua_but.Text = "Chỉnh sửa";
                tt_hoten_txt.Enabled = false;
                tt_ngaysinh_txt.Enabled = false;
                tt_diachi_txt.Enabled = false;
                tt_sdt_txt.Enabled = false;
            }
        }

        private void doi_matkhau_but_Click(object sender, EventArgs e)
        {
            if (doi_matkhau_but.Text == "Đổi mật khẩu")
            {
                Form6 f6 = new Form6();
                f6.ShowDialog();
                if (f6.matkhau_ == matkhau_)
                {
                    matkhaumoi1_lab.Visible = true;
                    matkhaumoi2_lab.Visible = true;
                    matkhaumoi1_txt.Visible = true;
                    matkhaumoi2_txt.Visible = true;
                    matkhaumoi1_txt.Text = "";
                    matkhaumoi2_txt.Text = "";
                    doi_matkhau_but.Text = "Xong";
                }
                else
                {
                    MessageBox.Show("Sai mật khẩu");
                }
            }
            else if (doi_matkhau_but.Text == "Xong")
            {
                if (matkhaumoi2_txt.Text != matkhaumoi1_txt.Text || matkhaumoi1_txt.Text.Length < 10)
                    MessageBox.Show("Mật khẩu không đủ độ dài hoặc nhập lại mật khẩu mới không khớp");
                else
                {
                    matkhau_ = matkhaumoi1_txt.Text;
                    byte[] salt = Convert.FromBase64String(thongtin[1]);
                    byte[] passphrase = Encoding.Unicode.GetBytes(matkhau_);
                    byte[] PassphraseAndSalt = new byte[salt.Length + passphrase.Length];
                    for (int i = 0; i < passphrase.Length; i++)
                        PassphraseAndSalt[i] = passphrase[i];
                    for (int i = passphrase.Length; i < passphrase.Length + salt.Length; i++)
                        PassphraseAndSalt[i] = salt[i - passphrase.Length];
                    PassphraseAndSalt = new SHA1CryptoServiceProvider().ComputeHash(PassphraseAndSalt);
                    thongtin[2] = BitConverter.ToString(PassphraseAndSalt).Replace("-", "");
                    File.WriteAllLines("Accounts\\" + this.Text.Replace("@", "_") + ".acc", thongtin);
                    doi_matkhau_but.Text = "Đổi mật khẩu";
                    matkhaumoi1_lab.Visible = false;
                    matkhaumoi2_lab.Visible = false;
                    matkhaumoi1_txt.Visible = false;
                    matkhaumoi2_txt.Visible = false;
                }
            }
        }

        private void thoat_but_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn thoát không", "Thoát", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                Application.Exit();
        }

        private void nx_xuatkhoa_but_Click(object sender, EventArgs e)
        {
            string[] xuat_thongtin = new string[8];
            SaveFileDialog sf = new SaveFileDialog();
            sf.Title = "Xuất thông tin khóa ra tập tin";
            sf.Filter = "Account File (*.acc)|*.acc|All File (*.*)|*.*";
            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                xuat_thongtin[0] = "Public Key: " + thongtin[7];
                xuat_thongtin[1] = "Private Key: " + thongtin[8];
                xuat_thongtin[2] = "Họ và tên: " + thongtin[3];
                xuat_thongtin[3] = "Ngày sinh: " + thongtin[4];
                xuat_thongtin[4] = "Địa chỉ: " + thongtin[5];
                xuat_thongtin[5] = "Số điện thoại: " + thongtin[6];
                xuat_thongtin[6] = "Giá trị Hash SHA1 (Mật khẩu + Salt): " + thongtin[2];
                xuat_thongtin[7] = "Giá trị Salt: " + thongtin[1];
                File.WriteAllLines(sf.FileName, xuat_thongtin);
                MessageBox.Show("Thành công");
            }
        }

        private void nx_nhapkhoa_but_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Title = "Nhập thông tin khóa từ tập tin";
            of.Filter = "Account File (*.acc)|*.acc|All File (*.*)|*.*";
            if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string[] nhap_thongtin = File.ReadAllLines(of.FileName);
                thongtin[1] = nhap_thongtin[7].Replace("Giá trị Salt: ", "");
                thongtin[2] = nhap_thongtin[6].Replace("Giá trị Hash SHA1 (Mật khẩu + Salt): ", "");
                thongtin[3] = nhap_thongtin[2].Replace("Họ và tên: ", "");
                thongtin[4] = nhap_thongtin[3].Replace("Ngày sinh: ", "");
                thongtin[5] = nhap_thongtin[4].Replace("Địa chỉ: ", "");
                thongtin[6] = nhap_thongtin[5].Replace("Số điện thoại: ", "");
                thongtin[7] = nhap_thongtin[0].Replace("Public Key: ", "");
                thongtin[8] = nhap_thongtin[1].Replace("Private Key: ", "");
                File.WriteAllLines("Accounts\\" + this.Text.Replace("@", "_") + ".acc", thongtin);
            }
        }

        private void mh_but_Click(object sender, EventArgs e)
        {
            // Tao khoa ngau nhien
            Rijndael SecretKey = new RijndaelManaged();
            SecretKey.KeySize = 256;

            // Chon mode of operation: mac dinh la ECB
            if (mh_moo_combo.Text == "CBC")
                SecretKey.Mode = CipherMode.CBC;
            else if (mh_moo_combo.Text == "OFB")
                SecretKey.Mode = CipherMode.OFB;
            else if (mh_moo_combo.Text == "CFB")
                SecretKey.Mode = CipherMode.CFB;
            else if (mh_moo_combo.Text == "CTS")
                SecretKey.Mode = CipherMode.CTS;
            else
                SecretKey.Mode = CipherMode.ECB;
            
            // Chon padding scheme
            if (mh_ps_combo.Text == "X923")
                SecretKey.Padding = PaddingMode.ANSIX923;
            else if (mh_ps_combo.Text == "ISO10126")
                SecretKey.Padding = PaddingMode.ISO10126;
            else
                SecretKey.Padding = PaddingMode.PKCS7;
           
            // Tao IV va Key
            SecretKey.GenerateIV();
            SecretKey.GenerateKey();
        }

        private void mh_duyet_but_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Title = "Chọn tập tin cần mã hóa";
            of.Multiselect = true;
            of.Filter = "All Files (*.*)|*.*";
            if (of.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                files = of.FileNames;
                for (int i = 0; i < files.Length; i++)
                {
                    mh_f_nhap_txt.Text += files[i] + " ";
                }
            }
        }
    }
}
