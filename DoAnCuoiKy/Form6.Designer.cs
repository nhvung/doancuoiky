﻿namespace DoAnCuoiKy
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form6));
            this.label1 = new System.Windows.Forms.Label();
            this.matkhau_txt = new System.Windows.Forms.TextBox();
            this.ok_but = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mật khẩu hiện tại:";
            // 
            // matkhau_txt
            // 
            this.matkhau_txt.Location = new System.Drawing.Point(110, 37);
            this.matkhau_txt.Name = "matkhau_txt";
            this.matkhau_txt.Size = new System.Drawing.Size(253, 20);
            this.matkhau_txt.TabIndex = 1;
            this.matkhau_txt.UseSystemPasswordChar = true;
            // 
            // ok_but
            // 
            this.ok_but.Location = new System.Drawing.Point(369, 35);
            this.ok_but.Name = "ok_but";
            this.ok_but.Size = new System.Drawing.Size(75, 23);
            this.ok_but.TabIndex = 2;
            this.ok_but.Text = "OK";
            this.ok_but.UseVisualStyleBackColor = true;
            this.ok_but.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form6
            // 
            this.AcceptButton = this.ok_but;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 87);
            this.Controls.Add(this.ok_but);
            this.Controls.Add(this.matkhau_txt);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form6";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập mật khẩu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox matkhau_txt;
        private System.Windows.Forms.Button ok_but;
    }
}